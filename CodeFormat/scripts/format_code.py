#!/usr/bin/env python3
import subprocess
import os
import argparse


def configFile2List(configFile):
    finalList = []
    with open(configFile) as f:
        for line in f.readlines():
            # ignore empty line and phrases after #
            lineStr = line.split('#')[0].strip()
            if lineStr == "":
                continue
            finalList.append(lineStr)
    return finalList


# The basic block match check which split by '/'. The '*' matches anything and the '?' match any one character. No '\' supported yet
def matchReg(sBlock, rBlock):
    # empty str case
    if rBlock == '':
        return sBlock == ''

    # sBlock = "abcdefg.pyc", rBlock = "*cd*.py*c". We first split rBlock using '*', then loop the list.
    # For each items, if '' then we skip it, or we should use find for it.
    # If match, we remove the str pattern.
    rParts = rBlock.split('*')
    for index in range(len(rParts)):
        part = rParts[index]
        # Special treatment for the first case and last case. If not '', means it's like 'abc*'(first) or '*def'(last), use == not find
        if index == 0:
            if part != '':
                if len(sBlock) < len(part):
                    return False
                for i in range(len(part)):
                    if part[i] != sBlock[i] and part[i] != '?':
                        return False
                sBlock = sBlock[len(part):]
        elif index == (len(rParts) - 1):
            if part != '':
                if len(sBlock) < len(part):
                    return False
                for i in range(-1, -len(part) - 1, -1):
                    if part[i] != sBlock[i] and part[i] != '?':
                        return False
        # Other cases, use find
        else:
            # "**" case, current version we don't do anything for it
            if part == '':
                continue
            # Count how many '?' before another character. You write '?*' or '*??*' ??!! It's usually meaningless!
            qMarkStartNum = 0
            initialCharacter = None
            for p in part:
                if p == '?':
                    qMarkStartNum += 1
                else:
                    initialCharacter = p
                    break
            if len(sBlock) < qMarkStartNum:
                return False
            sBlock = sBlock[qMarkStartNum:]
            if initialCharacter is not None:
                noMatch = True
                finalPosi = -1
                while noMatch:
                    finalPosi = sBlock.find(initialCharacter, finalPosi + 1)
                    if finalPosi == -1:
                        return False
                    else:
                        for i in range(len(part)):
                            if i + finalPosi >= len(sBlock): return False
                            if part[i] != sBlock[i + finalPosi] and part[i] != '?':
                                # not match whole, beak this for loop and exclude strs before and on this posi next time
                                noMatch = True
                                break
                            # By this strategy, the noMatch will be False after the for loop only the case that all of the char are matched
                            noMatch = False
                # Remove the matched strs after successful match in case they will be matched by the following reg block
                sBlock = sBlock[finalPosi + len(part):]
    # The loop is finished with no issue. We are all matched and return true
    return True


# An asterisk "*" matches anything except "/". The character "?" matches any one character except "/"
# If there is a "/" at the end of the pattern then the pattern will only match directories. No '\' supported yet
def simpleRegex(targetStr, reg):
    # two cases: no '/', '/' only at the end. Or have '/' in middle
    # If have '/' in middle, each '/' split block size should match regex
    # Remove the last '/' if we have
    if reg[-1] == '/':
        if not os.path.isdir(targetStr):
            return False
        else:
            reg.rstrip('/')
            if targetStr[-1] == '/':
                targetStr.rstrip('/')

    # There are separator in the middle
    if reg.find('/') != -1:
        strBlocks = targetStr.split('/')
        regexBlocks = reg.split('/')
        if len(strBlocks) != len(regexBlocks):
            return False
        for index in range(len(strBlocks)):
            if not matchReg(strBlocks[index], regexBlocks[index]):
                return False
        return True
    else:
        strBlocks = targetStr.split('/')
        # Loop all of the str blocks. If the one matched the reg, then we passed and return true
        for block in strBlocks:
            if matchReg(block, reg):
                return True
    return False


'''
Similar as the .gitignore with ONLY these rules applied
If there is a separator at the beginning or middle (or both) of the pattern, then the pattern is relative to the directory
level of the particular .gitignore file itself. Otherwise the pattern may also match at any level below the .gitignore level.
If there is a separator at the end of the pattern then the pattern will only match directories, otherwise the pattern can match both files and directories.
For example, a pattern doc/frotz/ matches doc/frotz directory, but not a/doc/frotz directory; however frotz/ matches 
frotz and a/frotz that is a directory (all paths are relative from the .gitignore file).
An asterisk "*" matches anything except a slash. The character "?" matches any one character except "/".
No '\' like feature support yet
'''


def vetoRegex(fileStr, vList, vConfigFile):
    if fileStr.isspace() or fileStr == "":
        return ""
    # If veto list is not None, will use vetoList, or will use the vetoConfigFile
    vetoL = None
    if (vList is not None) and (len(vList) != 0):
        vetoL = vList
    elif vConfigFile is not None:
        vetoL = configFile2List(vConfigFile)
    else:
        return fileStr

    fileLists = fileStr.split(' ')
    outList = []
    for fileName in fileLists:
        if fileName.isspace() or fileName == "":
            continue
        matched = False
        for v in vetoL:
            # do the special treatment for the  '/', '*' and '?'
            if v.find('/') != -1 or v.find('*') != -1 or v.find('?') != -1:
                matched = simpleRegex(fileName, v)
            # if the re without these info, we could just do a simple find
            else:
                words = fileName.split('/')
                for w in words:
                    if w == v:
                        matched = True
                        break
            # if already matched, break the loop to save time
            if matched:
                break
        if not matched:
            outList.append(fileName)
    return ' '.join(outList)


CppFiles = ""
PythonFiles = ""

parser = argparse.ArgumentParser("format the code to the defined style")
parser.add_argument("-i", "--inputs", dest="inputs", nargs='+', help="The files or folders want to check")
parser.add_argument(
    "-v",
    "--vetoList",
    dest="vetoList",
    nargs='*',
    help="The regex list to veto files you don't want to do the code format")
parser.add_argument(
    "-f",
    "--vetoConfigFile",
    dest="vetoConfigFile",
    help="The file which contains the regex list to veto files you don't want to do the code format")
args = parser.parse_args()

scriptPath = os.path.dirname(os.path.realpath(__file__))

inputs = args.inputs
vetoList = args.vetoList
vetoConfigFile = args.vetoConfigFile

for file_or_folder in inputs:
    print("finding cxx files and python files in ", file_or_folder)
    CppFiles += " " + subprocess.getoutput(
        "find " + file_or_folder +
        " -type f -name '*' | egrep '\.cxx$|\.cpp$|\.c$|\.cc$|\.C$|\.h$|\.H$|\.hpp$|\.hh$|\.hxx$|\.ixx$' | xargs")
    PythonFiles += " " + subprocess.getoutput("find " + file_or_folder + " -type f -name '*' | grep '\.py$' | xargs")

finalCppFiles = vetoRegex(CppFiles, vetoList, vetoConfigFile)
finalPythonFiles = vetoRegex(PythonFiles, vetoList, vetoConfigFile)

if finalCppFiles != "":
    print("formatting all found cxx files")
    subprocess.getoutput("clang-format -style=file -i " + finalCppFiles)

if finalPythonFiles != "":
    print("formatting all found python files")
    subprocess.getoutput("yapf --style " + scriptPath + "/../style/python.style -i " + finalPythonFiles)
