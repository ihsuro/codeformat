FROM debian:stable-slim

RUN apt-get update && \
    apt-get install -y openssh-client clang-format git python3 python3-pip && \
	python3 -m pip install yapf
COPY CodeFormat/style/cpp.style /.clang-format
COPY CodeFormat /CodeFormat
CMD bash
